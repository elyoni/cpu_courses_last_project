# ====================================================================
#
#	File Name:		vsim_part3.do
#	Description:	This file is the Do file to run ModelSim compilation and analysis.
#
#	Date:			15/04/2013
#	Designer:		Assaf Zelinger
#
# ====================================================================

set top ..
set path_to_quartus D:/altera/11.1sp2/quartus		
set Resolution ns
set UserTimeUnit us

alias h {

echo "================= Commands List =================="
echo "													"
echo "	gen_libs	-	Generate Libraries				"
echo " 	comp_libs	-	Compile Libraries				"
echo "	comp_all	-	Compile All Files				"
echo "	comp_rtl	- 	Compile RTL files				"
echo "	comp_tb		- 	Compile Test Bench files		"
echo "	sim_lab		-	Start lab Test Bench Simulation		"
echo " 	ld_do_lab		-	Load lab Do File					"
echo "	del_libs	-	Delete Work Library				"
echo "	rerun #		- 	Restart and Run # us			"
echo "	h		-	Print Help Menu					"
echo "													"
echo "=================================================="

}


alias comp_rtl {
	echo "Compilng RTL Files:"
	
	echo "Compiling part3.vhd"
	vcom -work $top/sim/work -93 -quiet -novopt $top/rtl/part3.vhd
	
}



alias comp_tb {

	echo "Compilng Test Bench Files:"
	
	echo "Compiling tb_func_pack.vhd"
	vcom -work $top/sim/work -93 -quiet -novopt $top/tb/tb_func_pack.vhd
	echo "Compiling Test Bench File: part3_tb.vhd"
	vcom -work $top/sim/work -93 -quiet -novopt $top/tb/part3_tb.vhd
	
}

alias comp_all {

	comp_rtl
	comp_tb
	
	h

}

alias sim_lab {

	vsim -voptargs=+acc -t fs part3_tb

}


alias ld_do_lab {

	do wave.do	

}
 
alias gen_libs {
	
	vlib -quiet $top/sim/work
	vmap -quiet work $top/sim/work	
	
	h
}

alias del_libs {	
	vdel -all
	vlib -quiet $top/sim/work
	vmap -quiet work $top/sim/work	
	
	h
}

proc rerun {time_var} {restart -force; run $time_var us}

h

