onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /part3_tb/I0/reset
add wave -noupdate -radix hexadecimal /part3_tb/I0/CLOCK_50
add wave -noupdate -divider {New Divider}
add wave -noupdate -radix hexadecimal /part3_tb/I0/read_ready
add wave -noupdate -radix hexadecimal /part3_tb/I0/write_ready
add wave -noupdate -radix hexadecimal /part3_tb/I0/read_s
add wave -noupdate -radix hexadecimal /part3_tb/I0/write_s
add wave -noupdate -radix hexadecimal /part3_tb/I0/read_s_out
add wave -noupdate -radix hexadecimal /part3_tb/I0/write_s_out
add wave -noupdate -radix hexadecimal /part3_tb/I0/readdata_left
add wave -noupdate -radix hexadecimal /part3_tb/I0/noise
add wave -noupdate -radix hexadecimal /part3_tb/I0/readdata_left_noisy
add wave -noupdate -radix hexadecimal /part3_tb/I0/writedata_left
add wave -noupdate -divider {New Divider}
add wave -noupdate -radix hexadecimal /part3_tb/I0/buffer_left(0)
add wave -noupdate -radix hexadecimal /part3_tb/I0/buffer_left(1)
add wave -noupdate -radix hexadecimal /part3_tb/I0/buffer_left(2)
add wave -noupdate -radix hexadecimal /part3_tb/I0/buffer_left(3)
add wave -noupdate -radix hexadecimal /part3_tb/I0/buffer_left(4)
add wave -noupdate -radix hexadecimal /part3_tb/I0/buffer_left(5)
add wave -noupdate -radix hexadecimal /part3_tb/I0/buffer_left(6)
add wave -noupdate -radix hexadecimal /part3_tb/I0/WL0
add wave -noupdate -radix hexadecimal /part3_tb/I0/WL1
add wave -noupdate -radix hexadecimal /part3_tb/I0/WL2
add wave -noupdate -radix hexadecimal /part3_tb/I0/WL3
add wave -noupdate -radix hexadecimal /part3_tb/I0/WL4
add wave -noupdate -radix hexadecimal /part3_tb/I0/WL5
add wave -noupdate -radix hexadecimal /part3_tb/I0/WL6
add wave -noupdate -radix hexadecimal /part3_tb/I0/WL_P
add wave -noupdate -divider {New Divider}
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {582734862 fs} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits fs
update
WaveRestoreZoom {432658669 fs} {589030741 fs}
