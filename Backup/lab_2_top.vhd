
library ieee;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_signed.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;



entity lab_2_top is 

	port (
				
		clk					: in 	std_logic;
		rst					: in 	std_logic;
		
		sw					: in	std_logic_vector (2 downto 0);
		
		hex1				: OUT	std_logic_vector (6 downto 0);
		hex2				: OUT	std_logic_vector (6 downto 0);
		hex3				: OUT	std_logic_vector (6 downto 0);
		hex4				: OUT	std_logic_vector (6 downto 0);
		
		writedata_left		: out	std_logic_vector (23 downto 0);
		writedata_right		: out	std_logic_vector (23 downto 0);
		write_s				: out	std_logic;
	
		readdata_left		: in	std_logic_vector (23 downto 0);
		readdata_right		: in	std_logic_vector (23 downto 0);
		read_s				: out	std_logic
		
	);
end lab_2_top;




architecture beh of lab_2_top is

type Sine_Value_Hex is array (0 to 47) of std_logic_vector(15 downto 0);
signal sine_Hex : Sine_Value_Hex :=(x"0000",x"10B4",x"2120",x"30FB",x"3FFF",x"4DEB",x"5A81",x"658B",x"6ED9",x"7640",x"7BA2",x"7EE6",x"7FFF",x"7EE6",x"7BA2",x"7640",x"6ED9",x"658B",x"5A81",x"4DEB",x"3FFF",x"30FB",x"2120",x"10B4",x"0000",x"EF4B",x"DEE0",x"CF05",x"C001",x"B215",x"A57E",x"9A74",x"9127",x"89BF",x"845D",x"8119",x"8000",x"8119",x"845D",x"89BF",x"9127",x"9A74",x"A57E",x"B215",x"C000",x"CF05",x"DEE0",x"EF4B");

signal	i						: integer range 0 to 47:= 0;
signal Number1,Number2			:	std_logic_vector(7 downto 0):= x"00";	
signal Num1_Sig,Num2_Sig		:	std_logic_vector(7 downto 0):= x"00";	

signal MaxSum_Vector			: std_logic_vector(23 downto 0);
--signal done						:std_logic;
--signal Sample_Counter 		: integer range 0 to 100 := 0;

signal Phase					: std_logic_vector (15 downto 0);
signal Max_Value				:std_logic_vector (40 downto 0);
signal Enable					: std_logic;

component seven_segment_encoder

	port (
	
		clk			: in	std_logic;
		rst			: in	std_logic;
		
		number	 	: in 	std_logic_vector (8-1 downto 0);
		
		digit_lsb	: out	std_logic_vector (6 downto 0);
		digit_msb	: out	std_logic_vector (6 downto 0)
		
	);
end component;

component conv
	-- generic (	
		-- number_Of_Sample : integer :=48;
	-- );
	port (
				
		clk					: in 	std_logic;
		rst					: in 	std_logic;
		Enable				: in	std_logic;
		
		Mic_Data_Input		: in	std_logic_vector (23 downto 0);
			
		Max_Value			: out	std_logic_vector (40 downto 0);
		Phase				: out	std_logic_vector (15 downto 0);
		done				: out 	std_logic
	);
end component;
	
	begin------------------Begin architecture

	seven_segment_encoder1	:	seven_segment_encoder			
		port map(
			clk				=> clk,
			rst				=> rst,
			
			number	 		=> x"0b",
			
			digit_lsb		=> hex1,
			digit_msb		=> hex2
		);	
	seven_segment_encoder2	:	seven_segment_encoder			
		port map(
			clk				=> clk,
			rst				=> rst,
			
			number	 		=> x"b0",
			
			digit_lsb		=> hex3,
			digit_msb		=> hex4
		);	
		
	convolator				:	conv
		port map(
			clk					=> clk,				--: in 	std_logic;
			rst					=> rst,				--: in 	std_logic;
			Enable				=> Enable,			--: in	std_logic;
			
			Mic_Data_Input		=> readdata_left,	--: in	std_logic_vector (2 downto 0);
				
			Max_Value			=> Max_Value,		--: out	std_logic_vector (23 downto 0);
			Phase				=> Phase,			--: out	std_logic_vector (15 downto 0);
			done				=> open				--: out 	std_logic
		);
		process (clk, rst)

			begin

				if (rst = '0') then

					writedata_left	<= (others => '0');
					writedata_right	<= (others => '0');
					read_s 			<= '0';
					write_s 		<= '0';
					i <= 0;			
				elsif (rising_edge (clk)) then
					
					case sw is
					
						when "000" =>
							-- Number1 <= x"01"; --sine_Hex(i)(7 downto 0);
							-- Number2 <= x"00"; --sine_Hex(i)(7 downto 0);						
							writedata_left	<= readdata_left;
							writedata_right <= readdata_right;
							read_s 			<= '1';
							write_s 		<= '1';
							Enable			<= '0';
						when "001" =>
							-- Number1 <= x"02"; --sine_Hex(i)(7 downto 0);
							-- Number2 <= x"00"; --sine_Hex(i)(7 downto 0);						
							writedata_left	<= readdata_right;
							writedata_right <= readdata_left;
							read_s 			<= '1';
							write_s 		<= '1';
							Enable			<= '0';
						when "010" =>
							-- Number1 <= x"03"; --sine_Hex(i)(7 downto 0);
							-- Number2 <= x"00"; --sine_Hex(i)(7 downto 0);						
							writedata_left	<= (others => '0');
							writedata_right <= readdata_right;
							read_s 			<= '1';
							write_s 		<= '1';
							Enable			<= '0';
						when "011" =>
							-- Number1 <= x"04"; --sine_Hex(i)(7 downto 0);
							-- Number2 <= x"00"; --sine_Hex(i)(7 downto 0);						
							writedata_left	<= readdata_left;
							writedata_right <= (others => '0');
							read_s 			<= '1';
							write_s 		<= '1';
							Enable			<= '0';
						when "100" =>
							-- Number1 <= x"05"; --sine_Hex(i)(7 downto 0);
							-- Number2 <= x"00"; --sine_Hex(i)(7 downto 0);						
							writedata_left	<= (others => '0');
							writedata_right <= (others => '0');
							read_s 			<= '0';
							write_s 		<= '0';
							Enable			<= '0';
						when "111" =>
							--done <= '1';
							writedata_left	<= sine_Hex(i) & x"00";--std_logic_vector(to_unsigned(sine(i),writedata_left'length));
							writedata_right <= sine_Hex(i) & x"00";--std_logic_vector(to_unsigned(sine(i),writedata_left'length));
							read_s 			<= '1';	--Must To Be
							write_s 		<= '1';
							Enable			<= '1';
							--Increment i, and save i in the range
							i <= i + 1;
							if (i = 48) then
								i	<=	0;
							end if;
							
							
							-- if (Sample_Counter = 144) then
								-- Sample_Counter <= 0;
								-- enable := '1';
								-- Temp_Position	:= 0;
								-- MaxSum		 	<= 0;
								-- sum				<= 0;
								
							-- end if;							
						when others => null;
						
					end case;
				
				end if;
				
			end process;
		
		-- process (clk, rst)
		--Variable---------------------
		--variable Sample_Counter : integer;
		-- variable enable			: std_logic := '0';
		-- variable sum			: integer:=0;
		-- variable MaxSum			: integer;
		-- variable Temp_Position	: integer;
		-- variable Num1,Num2		: std_logic_vector(7 downto 0) := x"00";
			-- begin
				-- done <= '1';
				-- if (rst = '0') then
					-- Sample_Counter	<= 0;			
				-- elsif rising_edge(clk) then
					--Increment Sample_Counter, and save Sample_Counter in the range
					-- Sample_Counter <= Sample_Counter + 1;
					-- if (Sample_Counter = 144) then
						-- Sample_Counter <= 0;
						-- enable := '1';
						-- Temp_Position	:= 0;
						-- MaxSum		 	<= 0;
						-- sum				<= 0;
					-- end if;
					
					--Save To Data To Array
					-- if enable = '0' then
						-- Samples(Sample_Counter)	<= readdata_left;   -------------Reading Data
					-- elsif enable = '1' then
					
						-- Start The Convolution;
						-- for i in 0 to 95 loop --The first Option
							-- Temp_Position := i;
							-- while (Temp_Position > 47) loop
								-- Temp_Position := Temp_Position - 48;
							-- end loop;
							-- MaxSum <= (MaxSum + (to_integer(unsigned(Samples(i)))*to_integer(unsigned(sine_Hex(Temp_Position)))));
						-- end loop;
						
						-- Temp_Position:=0;
						-- for i in 0 to 95 loop --The other Options
							-- for j in 0 to 95 loop --Sample
								-- Temp_Position := (j+i);
								-- while (Temp_Position > 47) loop
									-- Temp_Position := Temp_Position - 48;
								-- end loop;
								-- sum <= (sum + (to_integer(unsigned(Samples(i)))*to_integer(unsigned(sine_Hex(Temp_Position)))));
							-- end loop;
							-- if sum > MaxSum then
								-- MaxSum <= sum;
							-- end if;
						-- end loop;			
						-- MaxSum_Vector <= std_logic_vector(to_unsigned(MaxSum,MaxSum_Vector'length));
						-- Num1_Sig <= MaxSum_Vector(7 downto 0);
						-- Num2_Sig <= MaxSum_Vector(15 downto 8);		--Print to Hex
						--done <= '1';
						-- enable := '0';
						
					-- end if;
				-- end if;
			-- end process;
end beh;


-- ====================================================================

	-- sw:
	
	-- 0 - rx to tx
	-- 1 - switch left and right
	-- 2 - mute left
	-- 3 - mute right
	-- 4 - mute all
	-- 5-7 resurved
		
	