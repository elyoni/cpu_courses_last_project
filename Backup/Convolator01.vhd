
library ieee;
use ieee.std_logic_unsigned.all;
--use ieee.std_logic_signed.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity conv is 
	generic (	
		number_Of_Sample : integer :=48
	);
	port (
				
		clk					: in 	std_logic;
		rst					: in 	std_logic;
		Enable				: in	std_logic;
		
		Mic_Data_Input		: in	std_logic_vector (23 downto 0);
			
		Max_Value			: out	std_logic_vector (40 downto 0);
		Phase				: out	std_logic_vector (15 downto 0);
		done				: out 	std_logic
	);
end conv;

architecture beh of conv is
----------------------------------------------------- Signal And types
type Sine_Value_Hex is array (0 to 47) of std_logic_vector(15 downto 0);
signal sine_Hex : Sine_Value_Hex :=(x"0000",x"10B4",x"2120",x"30FB",x"3FFF",x"4DEB",x"5A81",x"658B",x"6ED9",x"7640",x"7BA2",x"7EE6",x"7FFF",x"7EE6",x"7BA2",x"7640",x"6ED9",x"658B",x"5A81",x"4DEB",x"3FFF",x"30FB",x"2120",x"10B4",x"0000",x"EF4B",x"DEE0",x"CF05",x"C001",x"B215",x"A57E",x"9A74",x"9127",x"89BF",x"845D",x"8119",x"8000",x"8119",x"845D",x"89BF",x"9127",x"9A74",x"A57E",x"B215",x"C000",x"CF05",x"DEE0",x"EF4B");

type Sample_Values is array (0 to (number_Of_Sample-1)) of std_logic_vector(23 downto 0);
signal Samples 					: Sample_Values;


--signal	i				: std_logic := '0';


begin------------------Begin architecture
	--i <= '1';
	process(clk)
	variable Sample_Counter	:	integer range 0 to number_Of_Sample-1 := 0;
	variable done_Var		:	std_logic;
	variable MaxSum			:	integer; --std_logic_vector(40 downto 0);
	variable Temp_Position			:	integer;
	begin
		done <= done_Var;
		if rising_edge(clk) then
			if Enable = '1' then
				done_Var := '0';
				Samples(Sample_Counter) <= Mic_Data_Input;
				Sample_Counter := Sample_Counter + 1;
				if (Sample_Counter = number_Of_Sample-1) then
			--reset Sample_Counter
					Sample_Counter := 0;
			--reach the maximum value, stop save sample and calculate
					done_Var := '1';
					for i in 0 to (number_Of_Sample-1) loop --The first Option
						Temp_Position := i;
						while (Temp_Position > 47) loop
							Temp_Position := Temp_Position - 48;
						end loop;
						--MaxSum := (MaxSum + (to_integer(unsigned(Samples(i)))*to_integer(unsigned(sine_Hex(Temp_Position)))));
						MaxSum := (MaxSum + (Samples(i)*sine_Hex(Temp_Position)));
						
					end loop;
					
					for i in 0 to (number_Of_Sample-1) loop --The other Options
						for j in 0 to (number_Of_Sample-1) loop --Sample
							Temp_Position := (j+i);
							while (Temp_Position > 47) loop
								Temp_Position := Temp_Position - 48;
							end loop;													--while (Temp_Position > 47) loop
							sum <= (sum + (Samples(i)*sine_Hex(Temp_Position)));
						end loop;  														--for j in 0 to (number_Of_Sample-1) loop
						if sum > MaxSum then
							MaxSum <= sum;
						end if;
					end loop;															--for i in 0 to (number_Of_Sample-1) loop			
					
					Max_Value	<= std_logic_vector(to_unsigned(MaxSum,Max_Value'length));
				end if; --if (Sample_Counter = number_Ofgit _Sample-1) then
			end if; -- if enable then
		end if; --if rising_edge(clk) then
	end process;
end beh;