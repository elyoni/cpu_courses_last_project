-- ====================================================================
--
--	File Name:		lab2_top_tb.vhd
--	Description:	this file is the testbench for the top level entity for lab 2
--					
--
--	Date:			06/04/2013
--	Designer:		Assaf Zelinger
--
-- ====================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_textio.all;
use std.textio.all;

use work.tb_func_pack.all;

entity lab2_top_tb is 
end lab2_top_tb;

architecture beh of lab2_top_tb is

	signal clk_50, rst_in, key0, key1, pll_locked : std_logic;
	signal hex0, hex1, hex2, hex3 : std_logic_vector (6 downto 0);
	signal hex0_sig, hex1_sig, hex2_sig, hex3_sig : integer;

	
	
	begin
	
		I0	:	entity work.part1
	
			port map (
			
				CLOCK_50		=> clk_50,
				AUD_DACLRCK		=> 
				AUD_ADCLRCK		=> 
				AUD_BCLK		=>
				AUD_ADCDAT		=>
			
				CLOCK_27		=> clk_27,
				KEY				=> key,		
				I2C_SDAT		=> sda,															
				I2C_SCLK		=> scl,
				AUD_DACDAT		=> AUD_DACDAT,
				AUD_XCK			=> AUD_XCK,


			);

		process 		-- generate clocks

			begin

				clk_50 <= '0', '1' after 10 ns;
				wait for 20 ns;
				

			end process;

		rst_in <= '1', '0' after 200 ns, '1' after 1 us;		-- generate external reset signal

		hex0_sig <= convert_seven_segment (hex0);
		hex1_sig <= convert_seven_segment (hex1);
		hex2_sig <= convert_seven_segment (hex2);
		hex3_sig <= convert_seven_segment (hex3);

	
		process

			variable l :line;
			variable bit_read :std_logic;
			file key0_stimolus: text open read_mode is "../sim/key0_stim_file.txt";

			begin
			
				key0 <= '0';
									
				wait until rising_edge (rst_in);
				
				wait_clks (100, clk_50mhz);
				
				report ("============================ start reading key0 data ============================");

				while not endfile(key0_stimolus) loop

					wait until rising_edge (clk_50mhz);

					readline(key0_stimolus, l);
					read(l,bit_read);					
					key0 <= bit_read;

				end loop;
				
				report ("============================ finished reading key0 data ============================");
				
				wait;	

			end process;
			
		process

			variable l :line;
			variable bit_read :std_logic;
			file key1_stimolus: text open read_mode is "../sim/key1_stim_file.txt";

			begin
			
				key1 <= '0';
									
				wait until rising_edge (rst_in);
				
				wait_clks (100, clk_50mhz);

				report ("============================ start reading key1 data ============================");
				
				while not endfile(key1_stimolus) loop

					wait until rising_edge (clk_50mhz);

					readline(key1_stimolus, l);
					read(l,bit_read);					
					key1 <= bit_read;

				end loop;
				
				report ("============================ finished reading key1 data ============================");
				
				wait;	

			end process;	

	end beh;
		
	