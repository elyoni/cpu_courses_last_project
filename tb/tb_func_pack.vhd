-- ====================================================================
--
--	File Name:		tb_func_pack.vhd
--	Description:	this file is a package for testbench functions
--					
--
--	Date:			09/02/2013
--	Designer:		Assaf Zelinger
--
-- ====================================================================


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package tb_func_pack is

	procedure wait_clks (num : in integer; signal clk : in std_logic);
	
	function convert_seven_segment (vector : std_logic_vector) return integer;
	
	
	
		
	
end tb_func_pack;

package body tb_func_pack is

	procedure wait_clks (num : in integer; signal clk : in std_logic) is
	
		begin
		
			if (num > 1) then
		
				for i in 0 to num loop
				
					wait until rising_edge (clk);
					
				end loop;
					
			else
			
				wait until rising_edge (clk);
			 
			end if;
			
		end wait_clks;
		
	function convert_seven_segment (vector : std_logic_vector) return integer is

		variable vector_int : std_logic_vector (6 downto 0);
		
		begin
		
			vector_int := vector; 
		
			case vector_int is
			
				when "1000000" => return 0;
				
				when "1111001" => return 1;
				
				when "0100100" => return 2;
				
				when "0110000" => return 3;
				
				when "0011001" => return 4;
				
				when "0010010" => return 5;
				
				when "0000010" => return 6;
				
				when "1111000" => return 7;
				
				when "0000000" => return 8;
				
				when "0010000" => return 9;
				
				when others => return 255;
				
			end case;
			
		end convert_seven_segment;
		
end tb_func_pack;
			