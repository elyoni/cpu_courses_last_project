library ieee;
use ieee.std_logic_unsigned.all;
--use ieee.std_logic_signed.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package sample_type is
    type Sine_Value_Hex is array (0 to 47) of std_logic_vector(15 downto 0);
end;