-- ====================================================================
--
--	File Name:		func_pack.vhd
--	Description:	this file is a package for functions in the project
--					
--
--	Date:			02/02/2013
--	Designer:		Assaf Zelinger
--
-- ====================================================================


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package func_pack is

	function seven_segment_hex (number : std_logic_vector) return std_logic_vector;
	-- this function recives a 4 bit number (0-9) and returns the 7 segment code for it in a 
	-- 7 bit vector where the bits represent:
	--			bit 6 - G
	--			bit 5 - F
	--			bit 4 - E
	--			bit 3 - D
	--			bit 2 - C
	--			bit 1 - B
	--			bit 0 - A
	-- see link: http://en.wikipedia.org/wiki/Seven-segment_display
		
	
end func_pack;

package body func_pack is

	function seven_segment_hex (number : std_logic_vector) return std_logic_vector is
	
		variable num_int : std_logic_vector (3 downto 0);
		
		begin
			
			num_int := number;
			
			case num_int is
			
				when x"0" => return "1000000"; --"0111111";
				when x"1" => return "1111001"; --"0000110";
				when x"2" => return "0100100"; --"1011011";
				when x"3" => return "0110000"; --"1001111";
				when x"4" => return "0011001"; --"1100110";
				when x"5" => return "0010010"; --"1101101";
				when x"6" => return "0000010"; --"1111101";
				when x"7" => return "1111000"; --"0000111";
				when x"8" => return "0000000"; --"1111111";
				when x"9" => return "0010000"; --"1101111";
				
				when x"A" => return "0001000"; --"1101111";
				when x"B" => return "0000011"; --"1101111";
				when x"C" => return "1000110"; --"1101111";
				when x"D" => return "0100001"; --"1101111";
				when x"E" => return "0000110"; --"1101111";
				when x"F" => return "0001110"; --"1101111";
				
				when others => return "0000000"; --"1000000";
				
			end case;
			
		end seven_segment_hex;
				
end func_pack;
			