library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.func_pack.all;

entity seven_segment_encoder is 

	generic (	
		size : integer :=8
	);
	port (
				
		clk			: in	std_logic;
		rst			: in	std_logic;
		
		number	 	: in 	std_logic_vector (size-1 downto 0);
		
		digit_lsb	: out	std_logic_vector (6 downto 0);
		digit_msb	: out	std_logic_vector (6 downto 0)
		
	);
end seven_segment_encoder;

architecture beh of seven_segment_encoder is

	begin
		process (clk, rst) 
			--variable number_calc : std_logic_vector (7 downto 0);
			begin
				if (rst = '0') then		--Reset The 7Seg.
					digit_lsb <= seven_segment_hex (x"0");
					digit_msb <= seven_segment_hex (x"0");		
				elsif (rising_edge (clk)) then
					digit_lsb <= seven_segment_hex (number (3 downto 0));
					digit_msb <= seven_segment_hex (number (size-1 downto 4));
				end if;	
				
			end process;
		
	end beh;
		
	