
library ieee;
use ieee.std_logic_unsigned.all;
--use ieee.std_logic_signed.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.dataType.all;

entity lab_2_top is 
	generic (	
		number_Of_Sample : integer :=144
	);
	port (
				
		clk					: in 	std_logic;
		CLOCK_27		: in 	std_logic;
		rst					: in 	std_logic;
		
		sw					: in	std_logic_vector (1 downto 0);
		
		hex1				: OUT	std_logic_vector (6 downto 0);
		hex2				: OUT	std_logic_vector (6 downto 0);
		hex3				: OUT	std_logic_vector (6 downto 0);
		hex4				: OUT	std_logic_vector (6 downto 0);
		
		writedata_left		: out	std_logic_vector (23 downto 0);
		writedata_right		: out	std_logic_vector (23 downto 0);
		write_s				: out	std_logic;
		
		 Sub_Max				: out std_logic_vector (39 downto 0);
	
		readdata_left 		: in	std_logic_vector (23 downto 0); --readdata_left
		readdata_right		: in	std_logic_vector (23 downto 0);
		read_s				: out	std_logic
		
	);
end lab_2_top;




architecture beh of lab_2_top is

type Sine_Value_Hex is array (0 to 47) of std_logic_vector(15 downto 0);
signal sine_Hex : Sine_Value_Hex :=(x"0000",x"10B4",x"2120",x"30FB",x"3FFF",x"4DEB",x"5A81",x"658B",x"6ED9",x"7640",x"7BA2",x"7EE6",x"7FFF",x"7EE6",x"7BA2",x"7640",x"6ED9",x"658B",x"5A81",x"4DEB",x"3FFF",x"30FB",x"2120",x"10B4",x"0000",x"EF4B",x"DEE0",x"CF05",x"C001",x"B215",x"A57E",x"9A74",x"9127",x"89BF",x"845D",x"8119",x"8000",x"8119",x"845D",x"89BF",x"9127",x"9A74",x"A57E",x"B215",x"C000",x"CF05",x"DEE0",x"EF4B");

type Sample_Values is array (0 to (number_Of_Sample-1)) of std_logic_vector(23 downto 0);
signal Samples 					: Sample_Values;
signal Samples_temp				: Sample_Values;

signal	i						: integer range 0 to 47:= 0;
signal Number1,Number2			: std_logic_vector(7 downto 0):= x"00";	
signal Num1_Sig,Num2_Sig		: std_logic_vector(7 downto 0):= x"00";	

signal MaxSum_Vector			: std_logic_vector(23 downto 0);
--signal done						:std_logic;
--signal Sample_Counter 		: integer range 0 to 100 := 0;
signal last_Max_Temp					: std_logic_vector(39 downto 0);
signal last_Max					: std_logic_vector(39 downto 0);
signal Sub_Max_Temp			:std_logic_vector (39 downto 0);


signal Phase						: std_logic_vector (15 downto 0);
signal Max_Value				: std_logic_vector (39 downto 0);

signal Enable					: std_logic;


signal Convolator_Status		: std_logic;
--signal readdata_left			: std_logic_vector (23 downto 0);


component seven_segment_encoder

	port (
	
		clk			: in	std_logic;
		rst			: in	std_logic;
		
		number	 	: in 	std_logic_vector (8-1 downto 0);
		
		digit_lsb	: out	std_logic_vector (6 downto 0);
		digit_msb	: out	std_logic_vector (6 downto 0)
		
	);
end component;

component conv
	-- generic (	
		-- number_Of_Sample : integer :=48;
	-- );
	port (
				
		clk					: in 	std_logic;
		rst					: in 	std_logic;
		Enable				: in	std_logic;
		
		Samples				: in	Sample_Values;
		
		
			
		Max_Value			: out	std_logic_vector (39 downto 0);
		Phase				: out	std_logic_vector (15 downto 0);
		done				: out 	std_logic
	);
end component;
	
	
begin------------------Begin architecture


	-- seven_segment_encoder1	:	seven_segment_encoder			
		-- port map(
			-- clk				=> clk,
			-- rst				=> rst,
			
			-- number	 		=> Number1,--x"0b",
			
			-- digit_lsb		=> hex1,
			-- digit_msb		=> hex2
		-- );	
	-- seven_segment_encoder2	:	seven_segment_encoder			
		-- port map(
			-- clk				=> clk,
			-- rst				=> rst,
			
			-- number	 		=> Number2,--x"b0",
			
			-- digit_lsb		=> hex3,
			-- digit_msb		=> hex4
		-- );	
		
	convolator				:	conv
		port map(
			clk					=> clk,				--: in 	std_logic;
			rst					=> rst,				--: in 	std_logic;
			Enable				=> Enable,			--: in	std_logic;
			
			Samples				=>Samples,
			
			--readdata_left		=> readdata_left,	--: in	std_logic_vector (2 downto 0);
				
			Max_Value			=> Max_Value,		--: out	std_logic_vector (23 downto 0);
			Phase				=> Phase,			--: out	std_logic_vector (15 downto 0);
			done				=> Convolator_Status	--: out 	std_logic
		);
	
		process (clk)
		variable sub : std_logic_vector (39 downto 0);
		begin
			if (last_Max_Temp>last_Max) then
				sub := last_Max_Temp-last_Max;
			else
				sub := last_Max-last_Max_Temp;
			end if;
			Sub_Max <= sub;
			if sub < x"500000" then
				hex1 <= "0001100";--"0011000";
				hex2 <= "1000000"; --"1111110";
				hex3 <= "0000111"; --"0001111"
				hex4 <=  "0010010"; --"1011011"
			else
				hex1 <= "0111111"; --"1011011"
				hex2 <= "0111111"; --"0001111"
				hex3 <= "0111111"; --"1111110";
				hex4 <= "0111111";				
			end if;
		end process;

		process (clk, rst) --clk
		variable Sample_Counter	:	integer range 0 to number_Of_Sample;
		variable last_Read			: std_logic_vector(23 downto 0);
			begin

				if (rst = '0') then
					Enable 			<= '0';
					writedata_left	<= (others => '0');
					writedata_right	<= (others => '0');
					read_s 			<= '0';
					write_s 		<= '0';
					i 				<= 0;
					-- for i in 0 to (number_Of_Sample - 1) loop
						-- Samples(i) <=x"000000";
					-- end loop;
					--Samples  <= (x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000");
--					Samples  <= (x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000",x"000000");
					Phase			 <= x"0000";
					last_Read		:= x"000000";
					--Sub_Max		<= x"0000000000";
				elsif (rising_edge (clk)) then
					case sw(1 downto 0) is
					
						when "00" =>			
							writedata_left	<= readdata_left;--readdata_left;
							writedata_right <= readdata_right;
							read_s 			<= '1';
							write_s 		<= '1';
							Enable			<= '0';
							Sample_Counter	:= 0;				
						when "10" =>
							--done <= '1';
							writedata_left	<= sine_Hex(i) & x"00";--std_logic_vector(to_unsigned(sine(i),writedata_left'length));
							writedata_right <= sine_Hex(i) & x"00";--std_logic_vector(to_unsigned(sine(i),writedata_left'length));
							read_s 			<= '1';	--Must To Be
							write_s 		<= '1';
							
						--Collect Data
												
							--if (Sample_Counter = number_Of_Sample) then
							if (Convolator_Status = '1') and (Sample_Counter = number_Of_Sample) then
								last_Max_Temp	<= Max_Value;
								last_Max				<= last_Max_Temp;
							
								Enable	<= '1';			--Start Conv.
								--Samples <= Samples_temp;
								Sample_Counter	:= 0;
							else
								if (last_Read /= readdata_left) then
									--Sub_Max <= readdata_left - last_Read;
									last_Read := readdata_left;
									Enable			<= '0';
									Samples(Sample_Counter) <= readdata_left;
									Sample_Counter := Sample_Counter + 1;								
								end if;									-- if last_Read /= readdata_left then
							end if;										--if (Sample_Counter = number_Of_Sample) then
						--Increment i, and save i in the range
							i <= i + 1;
							if (i = 48) then
								i	<=	0;
							end if;
	
						when others => 
							writedata_left	<= (others => '0');
							writedata_right <= (others => '0');
							read_s 			<= '0';
							write_s 		<= '0';
							Enable			<= '0';			
					end case;
				end if;
			end process;
end beh;


-- ====================================================================

	-- sw:
	
	-- 0 - rx to tx
	-- 1 - switch left and right
	-- 2 - mute left
	-- 3 - mute right
	-- 4 - mute all
	-- 5-7 resurved
		
	