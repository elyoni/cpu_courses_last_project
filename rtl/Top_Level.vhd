LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_signed.all;

ENTITY Top_Level IS
	PORT (	CLOCK_50,AUD_DACLRCK, AUD_ADCLRCK, AUD_BCLK,AUD_ADCDAT	:IN STD_LOGIC;
			sw														:IN std_logic_vector (3 downto 0);
			CLOCK_27												:IN STD_LOGIC_VECTOR(1 DOWNTO 1);
			KEY														:IN STD_LOGIC_VECTOR(0 DOWNTO 0);
			I2C_SDAT												:INOUT STD_LOGIC;
			I2C_SCLK,AUD_DACDAT,AUD_XCK								:OUT STD_LOGIC;
			hex1													:OUT std_logic_vector (6 downto 0);
			hex2													:OUT std_logic_vector (6 downto 0);
			hex3													:OUT std_logic_vector (6 downto 0);
			hex4													:OUT std_logic_vector (6 downto 0)
			);
END Top_Level;

ARCHITECTURE Behavior OF Top_Level IS
	COMPONENT clock_generator
		PORT(	CLOCK_27														:IN STD_LOGIC_VECTOR(1 DOWNTO 1);
		    	reset															:IN STD_LOGIC;
				AUD_XCK															:OUT STD_LOGIC);
	END COMPONENT;

	COMPONENT audio_and_video_config
		PORT(	CLOCK_50,reset												:IN STD_LOGIC;
		    	I2C_SDAT														:INOUT STD_LOGIC;
				I2C_SCLK														:OUT STD_LOGIC);
	END COMPONENT;	

	COMPONENT audio_codec
		PORT(	CLOCK_50,reset,read_s,write_s							:IN STD_LOGIC;
				writedata_left, writedata_right							:IN STD_LOGIC_VECTOR(23 DOWNTO 0);
				AUD_ADCDAT,AUD_BCLK,AUD_ADCLRCK,AUD_DACLRCK				:IN STD_LOGIC;
				read_ready, write_ready									:OUT STD_LOGIC;
				readdata_left, readdata_right							:OUT STD_LOGIC_VECTOR(23 DOWNTO 0); -- The Data From Mic Left And Right
				AUD_DACDAT												:OUT STD_LOGIC);
	END COMPONENT;
	
	COMPONENT noise_generator
		PORT(	CLOCK_50		:IN STD_LOGIC;
		    	read_s			:IN STD_LOGIC;
				noise			:OUT STD_LOGIC_VECTOR(23 DOWNTO 0));
	END COMPONENT;
	
	component Pll_Conv
	PORT
	(
		inclk0		: IN STD_LOGIC  := '0';
		c0			: OUT STD_LOGIC 
	);
end component;
	
	component lab_2_top 
	
		port (
		
			clk					: in 	std_logic;
			rst					: in 	std_logic;
			CLOCK_27		: in 	std_logic;
			
			sw					: in	std_logic_vector (1 downto 0);
			
			hex1				: OUT	std_logic_vector (6 downto 0);
			hex2				: OUT	std_logic_vector (6 downto 0);
			hex3				: OUT	std_logic_vector (6 downto 0);
			hex4				: OUT	std_logic_vector (6 downto 0);
			
			writedata_left		: out	std_logic_vector (23 downto 0);
			writedata_right		: out	std_logic_vector (23 downto 0);
			write_s				: out	std_logic;
		
			Sub_Max				: out std_logic_vector (39 downto 0);
		
			readdata_left 		: in	std_logic_vector (23 downto 0);--readdata_left
			readdata_right		: in	std_logic_vector (23 downto 0);
			read_s				: out	std_logic
			
		);
	end component;

	SIGNAL read_ready, write_ready, read_s, write_s		:STD_LOGIC; 
--	SIGNAL  read_s, write_s		:STD_LOGIC;
	SIGNAL readdata_left, readdata_right, readdata_left_int, readdata_right_int	:STD_LOGIC_VECTOR(23 DOWNTO 0);
	SIGNAL writedata_left, writedata_right				:STD_LOGIC_VECTOR(23 DOWNTO 0);	
	signal noise										:STD_LOGIC_VECTOR(23 DOWNTO 0);	
	SIGNAL reset										:STD_LOGIC;
	SIGNAL clock_pll									: std_logic;
	SIGNAL clock_27_T								: std_logic;
	signal 	Sub_Max				: std_logic_vector (39 downto 0);

BEGIN
	reset <= NOT(KEY(0));
	clock_27_T <= CLOCK_27(1);
	clk_pll		:	Pll_Conv
	port map(
		inclk0		=>	CLOCK_50,				--: IN STD_LOGIC  := '0';
		c0			=>	clock_pll			--: OUT STD_LOGIC ;
		--locked		=> 	'1'					--: OUT STD_LOGIC 
	);
	
	my_clock_gen: clock_generator PORT MAP (CLOCK_27(1 DOWNTO 1), reset, AUD_XCK);
	cfg: audio_and_video_config PORT MAP (CLOCK_50, reset, I2C_SDAT, I2C_SCLK);
	codec: audio_codec PORT MAP(CLOCK_50,reset,read_s,write_s,writedata_left, writedata_right,AUD_ADCDAT,AUD_BCLK,AUD_ADCLRCK,AUD_DACLRCK,read_ready, write_ready,readdata_left, readdata_right,AUD_DACDAT);
	--noise_gen: noise_generator PORT MAP (CLOCK_50, read_s, noise);	
	

	

	process (clock_pll, reset)
	
		begin		
			if (reset = '0') then	--no audio output
				readdata_left_int <= (others => '0');
				readdata_right_int <= (others => '0');			
			elsif (rising_edge (clock_pll)) then
				if (sw(0) = '0') then			
					readdata_left_int <= readdata_left;
					readdata_right_int <= readdata_right;				
				else			
					readdata_left_int <= (others => '0');
					readdata_right_int <= (others => '0');				
				end if;
			end if;

		end process;
	lab_2_top0	: lab_2_top 
		port map (
		
			clk					=> clock_pll,
			rst					=> not (reset),
			CLOCK_27		=> clock_27_T,--: in 	std_logic;
			sw					=> sw (2 downto 1),
			
			hex1				=> hex1,				--: OUT	std_logic_vector (6 downto 0);
			hex2				=> hex2,				--: OUT	std_logic_vector (6 downto 0);
			hex3				=> hex3,				--: OUT	std_logic_vector (6 downto 0);
			hex4				=> hex4,				--: OUT	std_logic_vector (6 downto 0);
			
			
			writedata_left		=> writedata_left,
			writedata_right		=> writedata_right,
			write_s				=> write_s,
		
			Sub_Max				=> Sub_Max,--: out std_logic_vector (23 downto 0);
		
			readdata_left		=> readdata_left,
			readdata_right		=> readdata_right_int,
			read_s				=> read_s
			
		);


END Behavior;
