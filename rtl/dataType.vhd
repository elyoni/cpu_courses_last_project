library ieee;
use ieee.std_logic_unsigned.all;
--use ieee.std_logic_signed.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package dataType is
	--variable number_Of_Sample : integer :=48;
    type Sample_Values is array (0 to (144-1)) of std_logic_vector(23 downto 0);
end;