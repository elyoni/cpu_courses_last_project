library ieee;
use ieee.std_logic_unsigned.all;
--use ieee.std_logic_signed.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--use work.sample_type.all;
use work.dataType.all;




entity conv is 
	generic (	
		number_Of_Sample : integer := 144
	);
	port (	
		clk					: in 	std_logic;
		rst					: in 	std_logic;
		Enable				: in	std_logic;
		
		Samples				: in	Sample_Values;
		--Mic_Data_Input		: in	std_logic_vector (23 downto 0);
			
		Max_Value			: out	std_logic_vector (39 downto 0);
		Phase				: out	std_logic_vector (15 downto 0);
		done				: out 	std_logic
	);
end conv;

architecture beh of conv is
	function rotate (x : integer) return integer is
	variable number : integer;
	begin
		number := x;
		if (number>47) then
			number	:= number - 48;
		end if;
		if (number>47) then
			number	:= number - 48;
		end if;
		if (number>47) then
			number	:= number - 48;
		end if;
		return number;
	end;

----------------------------------------------------- Signal And types
type Sine_Value_Hex is array (0 to 47) of std_logic_vector(15 downto 0);
signal sine_Hex : Sine_Value_Hex :=(x"0000",x"10B4",x"2120",x"30FB",x"3FFF",x"4DEB",x"5A81",x"658B",x"6ED9",x"7640",x"7BA2",x"7EE6",x"7FFF",x"7EE6",x"7BA2",x"7640",x"6ED9",x"658B",x"5A81",x"4DEB",x"3FFF",x"30FB",x"2120",x"10B4",x"0000",x"EF4B",x"DEE0",x"CF05",x"C001",x"B215",x"A57E",x"9A74",x"9127",x"89BF",x"845D",x"8119",x"8000",x"8119",x"845D",x"89BF",x"9127",x"9A74",x"A57E",x"B215",x"C000",x"CF05",x"DEE0",x"EF4B");
--type sum_array	is  array (0 to 47) of std_logic_vector(39 downto 0);

--signal Samples 					: Sample_Values;

--signal	clk			: std_logic;
signal	counter		: integer range 0 to (number_Of_Sample-1);-- 143; --need to be integer because i sum it with "i"

signal  temp_done	: std_logic;

--signal 	RealMaxSum		:	std_logic_vector(39 downto 0);--sum_array;
signal 	bufferMaxValue		:	std_logic_vector(39 downto 0);--sum_array;


-- component Pll_Conv
	-- PORT
	-- (
		-- inclk0		: IN STD_LOGIC  := '0';
		-- c0			: OUT STD_LOGIC ;
		-- locked		: OUT STD_LOGIC 
	-- );
-- end component;


begin------------------Begin architecture
done <= temp_done;




	process(clk,rst)
	--variable Sample_Counter	:	integer range 0 to number_Of_Sample-1 := 0;
	--variable done_Var		:	std_logic;
	variable MaxSum		:	std_logic_vector(39 downto 0);--sum_array;
	variable sum			:	std_logic_vector(39 downto 0);  --std_logic_vector(40 downto 0);
	--variable Temp_Position	:	integer range 0 to 47;
	--variable temp_done		:	std_logic;
	variable status			:	std_logic_vector(1 downto 0);
	begin
		--done <= done_Var;
		if rst = '0' then
			temp_done <= '1';
		else	
			if rising_edge(clk) then
				case status is
					when "01" => -- make the integ
						temp_done <= '0';
						--RealMaxSum <= x"0000000000";
						sum := (Samples(0)*sine_Hex(0));--);
						--sum := sum + (Samples(1)*sine_Hex(rotate(counter+1)));
						--sum := sum + (Samples(2)*sine_Hex(rotate(counter+2)));
						--sum := sum + (Samples(3)*sine_Hex(rotate(counter+3)));
						sum := sum + (Samples(4)*sine_Hex(rotate(counter+4)));
						--sum := sum + (Samples(5)*sine_Hex(rotate(counter+5)));
						--sum := sum + (Samples(6)*sine_Hex(rotate(counter+6)));
						--sum := sum + (Samples(7)*sine_Hex(rotate(counter+7)));
						sum := sum + (Samples(8)*sine_Hex(rotate(counter+8)));
						--sum := sum + (Samples(9)*sine_Hex(rotate(counter+9)));
						--sum := sum + (Samples(10)*sine_Hex(rotate(counter+10)));
						--sum := sum + (Samples(11)*sine_Hex(rotate(counter+11)));
						sum := sum + (Samples(12)*sine_Hex(rotate(counter+12)));
						--sum := sum + (Samples(13)*sine_Hex(rotate(counter+13)));
						--sum := sum + (Samples(14)*sine_Hex(rotate(counter+14)));
						--sum := sum + (Samples(15)*sine_Hex(rotate(counter+15)));
						sum := sum + (Samples(16)*sine_Hex(rotate(counter+16)));
						--sum := sum + (Samples(17)*sine_Hex(rotate(counter+17)));
						--sum := sum + (Samples(18)*sine_Hex(rotate(counter+18)));
						--sum := sum + (Samples(19)*sine_Hex(rotate(counter+19)));
						sum := sum + (Samples(20)*sine_Hex(rotate(counter+20)));
						--sum := sum + (Samples(21)*sine_Hex(rotate(counter+21)));
						--sum := sum + (Samples(22)*sine_Hex(rotate(counter+22)));
						--sum := sum + (Samples(23)*sine_Hex(rotate(counter+23)));
						sum := sum + (Samples(24)*sine_Hex(rotate(counter+24)));
						--sum := sum + (Samples(25)*sine_Hex(rotate(counter+25)));
						--sum := sum + (Samples(26)*sine_Hex(rotate(counter+26)));
						--sum := sum + (Samples(27)*sine_Hex(rotate(counter+27)));
						sum := sum + (Samples(28)*sine_Hex(rotate(counter+28)));
						--sum := sum + (Samples(29)*sine_Hex(rotate(counter+29)));
						--sum := sum + (Samples(30)*sine_Hex(rotate(counter+30)));
						--sum := sum + (Samples(31)*sine_Hex(rotate(counter+31)));
						sum := sum + (Samples(32)*sine_Hex(rotate(counter+32)));
						--sum := sum + (Samples(33)*sine_Hex(rotate(counter+33)));
						--sum := sum + (Samples(34)*sine_Hex(rotate(counter+34)));
						--sum := sum + (Samples(35)*sine_Hex(rotate(counter+35)));
						sum := sum + (Samples(36)*sine_Hex(rotate(counter+36)));
						--sum := sum + (Samples(37)*sine_Hex(rotate(counter+37)));
						--sum := sum + (Samples(38)*sine_Hex(rotate(counter+38)));
						--sum := sum + (Samples(39)*sine_Hex(rotate(counter+39)));
						sum := sum + (Samples(40)*sine_Hex(rotate(counter+40)));
						--sum := sum + (Samples(41)*sine_Hex(rotate(counter+41)));
						--sum := sum + (Samples(42)*sine_Hex(rotate(counter+42)));
						--sum := sum + (Samples(43)*sine_Hex(rotate(counter+43)));
						sum := sum + (Samples(44)*sine_Hex(rotate(counter+44)));
						--sum := sum + (Samples(45)*sine_Hex(rotate(counter+45)));
						--sum := sum + (Samples(46)*sine_Hex(rotate(counter+46)));
						--sum := sum + (Samples(47)*sine_Hex(rotate(counter+47)));
						sum := sum + (Samples(0)*sine_Hex(rotate(counter+48)));
						--sum := sum + (Samples(1)*sine_Hex(rotate(counter+1)));
						--sum := sum + (Samples(2)*sine_Hex(rotate(counter+2)));
						--sum := sum + (Samples(3)*sine_Hex(rotate(counter+3)));
						sum := sum + (Samples(4)*sine_Hex(rotate(counter+52)));
						--sum := sum + (Samples(5)*sine_Hex(rotate(counter+5)));
						--sum := sum + (Samples(6)*sine_Hex(rotate(counter+6)));
						--sum := sum + (Samples(7)*sine_Hex(rotate(counter+7)));
						sum := sum + (Samples(8)*sine_Hex(rotate(counter+56)));
						--sum := sum + (Samples(9)*sine_Hex(rotate(counter+9)));
						--sum := sum + (Samples(10)*sine_Hex(rotate(counter+10)));
						--sum := sum + (Samples(11)*sine_Hex(rotate(counter+11)));
						sum := sum + (Samples(12)*sine_Hex(rotate(counter+60)));
						--sum := sum + (Samples(13)*sine_Hex(rotate(counter+13)));
						--sum := sum + (Samples(14)*sine_Hex(rotate(counter+14)));
						--sum := sum + (Samples(15)*sine_Hex(rotate(counter+15)));
						sum := sum + (Samples(16)*sine_Hex(rotate(counter+64)));
						--sum := sum + (Samples(17)*sine_Hex(rotate(counter+17)));
						--sum := sum + (Samples(18)*sine_Hex(rotate(counter+18)));
						--sum := sum + (Samples(19)*sine_Hex(rotate(counter+19)));
						sum := sum + (Samples(20)*sine_Hex(rotate(counter+68)));
						--sum := sum + (Samples(21)*sine_Hex(rotate(counter+21)));
						--sum := sum + (Samples(22)*sine_Hex(rotate(counter+22)));
						--sum := sum + (Samples(23)*sine_Hex(rotate(counter+23)));
						sum := sum + (Samples(24)*sine_Hex(rotate(counter+72)));
						--sum := sum + (Samples(25)*sine_Hex(rotate(counter+25)));
						--sum := sum + (Samples(26)*sine_Hex(rotate(counter+26)));
						--sum := sum + (Samples(27)*sine_Hex(rotate(counter+27)));
						sum := sum + (Samples(28)*sine_Hex(rotate(counter+76)));
						--sum := sum + (Samples(29)*sine_Hex(rotate(counter+29)));
						--sum := sum + (Samples(30)*sine_Hex(rotate(counter+30)));
						--sum := sum + (Samples(31)*sine_Hex(rotate(counter+31)));
						sum := sum + (Samples(32)*sine_Hex(rotate(counter+80)));
						--sum := sum + (Samples(33)*sine_Hex(rotate(counter+33)));
						--sum := sum + (Samples(34)*sine_Hex(rotate(counter+34)));
						--sum := sum + (Samples(35)*sine_Hex(rotate(counter+35)));
						sum := sum + (Samples(36)*sine_Hex(rotate(counter+84)));
						--sum := sum + (Samples(37)*sine_Hex(rotate(counter+37)));
						--sum := sum + (Samples(38)*sine_Hex(rotate(counter+38)));
						--sum := sum + (Samples(39)*sine_Hex(rotate(counter+39)));
						sum := sum + (Samples(40)*sine_Hex(rotate(counter+88)));
						--sum := sum + (Samples(41)*sine_Hex(rotate(counter+41)));
						--sum := sum + (Samples(42)*sine_Hex(rotate(counter+42)));
						--sum := sum + (Samples(43)*sine_Hex(rotate(counter+43)));
						sum := sum + (Samples(44)*sine_Hex(rotate(counter+92)));
						--sum := sum + (Samples(45)*sine_Hex(rotate(counter+45)));
						--sum := sum + (Samples(46)*sine_Hex(rotate(counter+46)));
						--sum := sum + (Samples(47)*sine_Hex(rotate(counter+47)));
						sum := sum + (Samples(0)*sine_Hex(rotate(counter+96)));
						--sum := sum + (Samples(1)*sine_Hex(rotate(counter+1)));
						--sum := sum + (Samples(2)*sine_Hex(rotate(counter+2)));
						--sum := sum + (Samples(3)*sine_Hex(rotate(counter+3)));
						sum := sum + (Samples(4)*sine_Hex(rotate(counter+100)));
						--sum := sum + (Samples(5)*sine_Hex(rotate(counter+5)));
						--sum := sum + (Samples(6)*sine_Hex(rotate(counter+6)));
						--sum := sum + (Samples(7)*sine_Hex(rotate(counter+7)));
						sum := sum + (Samples(8)*sine_Hex(rotate(counter+104)));
						--sum := sum + (Samples(9)*sine_Hex(rotate(counter+9)));
						--sum := sum + (Samples(10)*sine_Hex(rotate(counter+10)));
						--sum := sum + (Samples(11)*sine_Hex(rotate(counter+11)));
						sum := sum + (Samples(12)*sine_Hex(rotate(counter+108)));
						--sum := sum + (Samples(13)*sine_Hex(rotate(counter+13)));
						--sum := sum + (Samples(14)*sine_Hex(rotate(counter+14)));
						--sum := sum + (Samples(15)*sine_Hex(rotate(counter+15)));
						sum := sum + (Samples(16)*sine_Hex(rotate(counter+112)));
						--sum := sum + (Samples(17)*sine_Hex(rotate(counter+17)));
						--sum := sum + (Samples(18)*sine_Hex(rotate(counter+18)));
						--sum := sum + (Samples(19)*sine_Hex(rotate(counter+19)));
						sum := sum + (Samples(20)*sine_Hex(rotate(counter+116)));
						--sum := sum + (Samples(21)*sine_Hex(rotate(counter+21)));
						--sum := sum + (Samples(22)*sine_Hex(rotate(counter+22)));
						--sum := sum + (Samples(23)*sine_Hex(rotate(counter+23)));
						sum := sum + (Samples(24)*sine_Hex(rotate(counter+120)));
						--sum := sum + (Samples(25)*sine_Hex(rotate(counter+25)));
						--sum := sum + (Samples(26)*sine_Hex(rotate(counter+26)));
						--sum := sum + (Samples(27)*sine_Hex(rotate(counter+27)));
						sum := sum + (Samples(28)*sine_Hex(rotate(counter+124)));
						--sum := sum + (Samples(29)*sine_Hex(rotate(counter+29)));
						--sum := sum + (Samples(30)*sine_Hex(rotate(counter+30)));
						--sum := sum + (Samples(31)*sine_Hex(rotate(counter+31)));
						sum := sum + (Samples(32)*sine_Hex(rotate(counter+128)));
						--sum := sum + (Samples(33)*sine_Hex(rotate(counter+33)));
						--sum := sum + (Samples(34)*sine_Hex(rotate(counter+34)));
						--sum := sum + (Samples(35)*sine_Hex(rotate(counter+35)));
						sum := sum + (Samples(36)*sine_Hex(rotate(counter+132)));
						--sum := sum + (Samples(37)*sine_Hex(rotate(counter+37)));
						--sum := sum + (Samples(38)*sine_Hex(rotate(counter+38)));
						--sum := sum + (Samples(39)*sine_Hex(rotate(counter+39)));
						sum := sum + (Samples(40)*sine_Hex(rotate(counter+136)));
						--sum := sum + (Samples(41)*sine_Hex(rotate(counter+41)));
						--sum := sum + (Samples(42)*sine_Hex(rotate(counter+42)));
						--sum := sum + (Samples(43)*sine_Hex(rotate(counter+43)));
						sum := sum + (Samples(44)*sine_Hex(rotate(counter+140)));
						--sum := sum + (Samples(45)*sine_Hex(rotate(counter+45)));
						--sum := sum + (Samples(46)*sine_Hex(rotate(counter+46)));
						--sum := sum + (Samples(47)*sine_Hex(rotate(counter+47)));							
						if MaxSum < sum then
							MaxSum := sum;
						end if;
						
						counter <= counter + 2;
						
						if (counter = number_Of_Sample) then
							status := "00";
							temp_done <= '1';
							--RealMaxSum <= MaxSum;
							Max_Value <= MaxSum;						
						end if;

					when others =>
						if Enable = '1'then
							status := "01";
							counter	<= 0;
							MaxSum := x"0000000000";
						end if;
				end case;
			end if; --if rising_edge(clk) then
		end if;		--if rst = '0' then
	end process;

	-- process(clk)
	-- begin
		-- if rising_edge(clk) then
			-- if (temp_done = '1') then
				-- bufferMaxValue <= RealMaxSum;
				-- Max_Value	<= bufferMaxValue;
			-- end if;
		-- end if;
	-- end process;
end beh;