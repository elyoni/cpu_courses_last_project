FIRSize=32;
%% All Pass Filter

AllPassFilter = fir1(FIRSize-1,[0.1 0.9]);
maxapf=max(max(AllPassFilter));
minapf=min(min(AllPassFilter));
AllPassFilter=floor(AllPassFilter/maxapf*127);
plot(AllPassFilter,'.')
figure
freqz(AllPassFilter,1,512)
title('All pass Filter');
AllPassFilterHex=char(' '*char(ones(FIRSize,2)));
AllPassFilterBin=char(' '*char(ones(FIRSize,8)));
for ii=1:size(AllPassFilter,2) 
    AllPassFilterHex(ii,:)=myhex(AllPassFilter(ii),2);
     AllPassFilterBin(ii,:)=dec2bin(hex2dec(AllPassFilterHex(ii,:)),8);
end

csvwrite('AllPassFilterDecimalValues.txt',AllPassFilter);%save data
csvwrite('AllPassFilterHexValues.txt',AllPassFilterHex);%save data
csvwrite('AllPassFilterBinaryValues.txt',AllPassFilterBin);%save data

%% band Pass Filter
BandPassFilter = fir1(FIRSize-1,[0.4 0.6]);
maxbpf=max(max(BandPassFilter));
minbpf=min(min(BandPassFilter));
BandPassFilter=floor(BandPassFilter/maxbpf*127);
plot(BandPassFilter,'.')
figure
freqz(BandPassFilter,1,512)
title('Band pass Filter');
BandPassFilterHex=char(' '*char(ones(FIRSize,2)));
BandPassFilterBin=char(' '*char(ones(FIRSize,8)));
for ii=1:size(AllPassFilter,2) 
    BandPassFilterHex(ii,:)=myhex(BandPassFilter(ii),2);
     BandPassFilterBin(ii,:)=dec2bin(hex2dec(BandPassFilterHex(ii,:)),8);
end

csvwrite('BandPassFilterDecimalValues.txt',BandPassFilter);%save data
csvwrite('BandPassFilterHexValues.txt',BandPassFilterHex);%save data
csvwrite('BandPassFilterBinaryValues.txt',BandPassFilterBin);%save data

%% High Pass Filter
HighPassFilter = fir1(FIRSize-1,0.9,'high');
maxhpf=max(max(HighPassFilter));
minhpf=min(min(HighPassFilter));
HighPassFilter=floor(HighPassFilter/maxhpf*127);
figure
plot(HighPassFilter,'.')
figure
freqz(HighPassFilter,1,512)
title('High pass Filter');
HighPassFilterHex=char(' '*char(ones(FIRSize,2)));
HighPassFilterBin=char(' '*char(ones(FIRSize,8)));
for ii=1:size(HighPassFilter,2) 
    HighPassFilterHex(ii,:)=myhex(HighPassFilter(ii),2);
     HighPassFilterBin(ii,:)=dec2bin(hex2dec(HighPassFilterHex(ii,:)),8);
end
csvwrite('HighPassFilterDecimalValues.txt',HighPassFilter);%save data
csvwrite('HighPassFilterHexValues.txt',HighPassFilterHex);%save data
csvwrite('HighPassFilterBinaryValues.txt',HighPassFilterBin);%save data

%% Low Pass Filter
LowPassFilter = 100*fir1(FIRSize-1,0.1,'low'); % cut frequency at 0.8
maxlpf=max(max(LowPassFilter));
minlpf=min(min(LowPassFilter));
LowPassFilter=floor(LowPassFilter/maxlpf*127);
figure
plot(LowPassFilter,'.')
figure
freqz(LowPassFilter,1,512)
title('Low pass Filter');
LowPassFilterHex=char(' '*char(ones(FIRSize,2)));
LowPassFilterBin=char(' '*char(ones(FIRSize,8)));
for ii=1:size(LowPassFilter,2) 
    LowPassFilterHex(ii,:)=myhex(LowPassFilter(ii),2);
     LowPassFilterBin(ii,:)=dec2bin(hex2dec(LowPassFilterHex(ii,:)),8);
end
csvwrite('LowPassFilterDecimalValues.txt',LowPassFilter);%save data
csvwrite('LowPassFilterHexValues.txt',LowPassFilterHex);%save data
csvwrite('LowPassFilterBinaryValues.txt',LowPassFilterBin);%save data
