function [mov,NumOfRow,NumOfCol] = CaptureMovie(Scale)
%This function reads in the raw movie and outputs a struct with its size   Detailed explanation goes here
%% capture the movie
xyloObj = VideoReader('../../RandomBackground.mj2');
% FramesToSkip=4;% if I want 8 frames
FramesToSkip=8;% if I want 4 frames
nFrames = floor(xyloObj.NumberOfFrames/FramesToSkip-2);
vidHeight = xyloObj.Height*Scale;
vidWidth = xyloObj.Width*Scale;
mov(1:nFrames) = ...
    struct('cdata',zeros(vidHeight,vidWidth, 3,'double'),...
    'colormap',[]);
for k = 1 : nFrames
    mov(k).cdata =uint8( 255*imresize(im2double(read(xyloObj,k*FramesToSkip)),Scale));
end
NumOfRow=size(mov(1).cdata,1);
NumOfCol=size(mov(1).cdata,2);
end