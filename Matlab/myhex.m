function [s]=myhex(i,n)
if i<0,
   s=dec2hex(int16((2^4)^n+i),n);
else
   s=dec2hex(i,n);
end